import { Component } from '@angular/core';

import { WeatherService } from './weather.service';

@Component({
    selector: 'app-weather',
    template: `
    <input type="text" [(ngModel)]="city" (keydown.enter)="getWeather()" placeholder="Enter City Here">
     <button (click)="getWeather()">send</button>
     <h2>{{ message }}</h2>
  `,
    styles: [`
    h2 {
        font-weight: 100;
    }
  `]
})

export class WeatherComponent {
    message: any;
    city: any;
    constructor(private weatherService: WeatherService) { }
    getWeather(): void {
        if (!this.city) {
            this.message = 'Insert city';
        } else {
            this.weatherService.getWeather(this.city)
                .subscribe(msg => {
                    this.message = 'Temperature in ' + msg.json().name + ' is    ' + msg.json().main.temp + ' °C';
                }, error => {
                    this.message = 'City not found !!!';
                });
        }
    }
}
