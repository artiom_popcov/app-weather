import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as constants from './constants';

@Injectable()
export class WeatherService {

    constructor(private http: Http) { }
    result: any;

    getWeather(parameter): any {
        return this.http
            .get(constants.URL_ADDRESS + parameter);
    }
}
